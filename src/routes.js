import welcome from './components/welcome.vue'
import signin from './components/login.vue'
import signup from './components/register.vue'
import forgetpassword from './components/resetpassword.vue'
import location from './components/location.vue'



export default [{
        path: '*',
        redirect: '/login'
    },
    {
        path: '/home',
        component: welcome,
        meta: {
            requireauth: true
        }
    },
    {
        path: '/login',
        component: signin
    },
    {
        path: '/register',
        component: signup
    },
    {
        path: '/forgetpassword',
        component: forgetpassword

    },
    {
        path: '/location',
        component: location,
        meta: {
            requireauth: true
        }

    }
]