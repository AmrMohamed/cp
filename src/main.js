import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Routes from './routes.js'
import { firebase } from './databaseconfig.js'
// import VueFire from 'vuefire'
import * as VueGoogleMaps from 'vue2-google-maps'


Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyBGEZxBpBiGn9pVm04d1-Y-vtMONlI9WOs',
        libraries: 'places', // This is required if you use the Autocomplete plugin
        // OR: libraries: 'places,drawing'
        // OR: libraries: 'places,drawing,visualization'
        // (as you require)
    }
});
Vue.use(VueRouter)
    // Vue.use(VueFire)

const router = new VueRouter({
    routes: Routes,
    mode: 'history'
});

router.beforeEach((to, from, next) => {
    let currentuser = firebase.auth().currentUser;
    let requireauth = to.matched.some(record => record.meta.requireauth);
    if (requireauth && !currentuser) {
        next('/login');
    } else if (!requireauth && currentuser) {
        next('/home');
    } else {
        next();
    }
});

firebase.auth().onAuthStateChanged((user) => {
    new Vue({
        el: '#app',
        render: h => h(App),
        router: router
    })
})